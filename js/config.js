var Config = {
    pivotal: {
        projectId: "ADDME_PROJECT_ID",
        token: "ADDME_TOKEN"
    },
    gravatar: {
        
    },
    
    urls: {
        project: "https://www.pivotaltracker.com/services/v3/projects/" + Config.pivotal.projectId,
    }
}