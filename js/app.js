/* global Config */

var App = Em.Application.create();

var Project = Em.Object.extend({
    name: undefined,
    url: undefined
});

var log = (function (output) {
    function func(type) {
        return function() {
            output[type || "log"].apply(output, 
            [].slice.call(arguments)
                .map(function(i) { 
                    return (typeof i === "object") ? JSON.stringify(i) : i; 
                })
             );
        };
    }

    var obj = func("log");
    obj.log = func("log");
    obj.error = func("error");
    obj.warn = func("warn");
    
    return obj;
})(console);

var Developer = Em.Object.extend({
    id: undefined,
    name: undefined,
    avatarUrl: undefined,
    currentTasks: []
    
//    currentTasksChanged: function() {
//        
//    }.observes("currentTasks"),
//    
//    avatarChanged: function() {
//        
//    }.observes("avatar")
});


App.View = {};

var Data = {
    project: {
        name: "PROJEKTI"
    },
    developers: [],

    fetchData: function() {
        var promises = [];
        
        promises.push(this.fetchProject());
        promises.push(this.fetchDevelopers()
            .then(function() {
                return this.fetchTasksForDevelopers();
            }));
    },

    fetchTasksForDevelopers: function() {
        var promises = [];
        Data.developers.forEach(function(dev) {
            var prom = $.ajax({
                url: Config.urls.project + "/stories?filter=mywork%3A" + dev.name,
                type: "GET",
                dataType: "xml",
                headers: {
                    "X-TrackerToken": Config.pivotal.token                
                }
            })
            .then(function(data) {
                dev.tasks = App.Parser.parseTasks(data);
            })
            .fail(function(err) {
                log.error("fetchTasksForDevelopers", err);
            });
            
            promises.push(prom);
        });
        
        return $.when.call(this, promises);
    },

    fetchProject: function() {
        return $.ajax({
            url: Config.urls.project,
            type: "GET",
            dataType: "xml",
            headers: {
                "X-TrackerToken": Config.pivotal.token                
            }
        })
        .then(function(data) {
            Data.project = App.Parser.parseProject(data);
        })
        .fail(function(err) {
            log.error("fetchProject", err);
        });
    },
    
    fetchDevelopers: function() {
        return $.ajax({
            url: Config.urls.project + "/memberships",
            type: "GET",
            dataType: "xml",
            headers: {
                "X-TrackerToken": Config.pivotal.token                
            }
        })
        .then(function(data) {
            var developers = [];
            
            var parseDeveloper = App.Parser.parseDeveloper;
            var memberships = data.querySelectorAll("membership");
            [].slice.call(memberships).forEach(function(m) {
               developers.push(parseDeveloper(m)); 
            });
            
            Data.developers = developers;
        })
        .fail(function(err) {
            log.error("fetchDevelopers", err);
        });
    }
}

App.Parser = (function() {

    function parseDeveloper(membership) {
        function getValue(selector) {
            return membership.querySelector(selector).textContent;
        }
    
        var dev = {};
        dev.id = getValue("id");
        dev.email = getValue("email");
        dev.name = getValue("name");
        dev.initials = getValue("initials");

        return dev;
    }
    
    return {
        parseDeveloper: parseDeveloper   
    }
})();

App.View.SummaryView = Em.View.extend({
    project: Project.create(Data.project)
});


App.DevelopersController = Em.ArrayController.create({
   content: Data.developers
});

//App.View.DeveloperListView = Em.CollectionView.extend({
//    tagName: "ul",
////    content: Data.developers,
//    itemViewClass: App.View.DeveloperView
//});


//App.View.DeveloperView = Em.View.extend({
//    templateName: "developer"
//});


//var statusView = App.View.StatusView.create({ 
//    project: Data.project
////    ,
////    developers: Data.developers
//});
//
//statusView.append();

//var anUndorderedListView = Ember.CollectionView.create({
//    tagName: 'ul',
//    content: [{ a: 'A' }, { a: 'B' }, { a: 'C' }],
//    itemViewClass: Ember.View.extend({
//      template: Ember.Handlebars.compile("the letter: {{content.a}}")
//    })
//  })
//
//  anUndorderedListView.appendTo('body')
